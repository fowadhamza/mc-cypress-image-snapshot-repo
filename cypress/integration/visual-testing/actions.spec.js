/// <reference types="cypress" />

context('Actions', () => {
  beforeEach(() => {
    cy.visit("/visualtesting.html")
    cy.viewport(1000,980)
  })

  // https://on.cypress.io/interacting-with-elements

  it('Visual Test Case Document', () => {
    cy.document().matchImageSnapshot();
  })

  it('Visual Test Case Initial State', () => {
    cy.get("canvas").matchImageSnapshot();
  })

  it('Visual Test Case Blue State', () => {
    cy.get("#blue").click();
    cy.get("canvas").matchImageSnapshot();
  })
  
  it('Visual Test Case Green State', () => {
    cy.get("#green").click();
    cy.get("canvas").matchImageSnapshot();
  })

  it('Visual Test Case Red State', () => {
    cy.get("#green").click();
    cy.get("canvas").matchImageSnapshot();
  })
})